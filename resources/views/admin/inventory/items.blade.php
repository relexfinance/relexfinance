@extends('admin.layout.master')

@section('title', 'Items')

@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <h1 class="mb-1">
                    Items
                </h1>
                <div class="card">
                    <div class="card-content" aria-expanded="true">
                        <div class="card-body">
                            @include('admin.inc.messages')
                            {{--page header--}}
                            <div class="row justify-content-center mb-5">
                                <div class="container">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group input-group">
                                                <div class="input-group-prepend">
													<span
                                                        class="input-group-text bg-primary bg-darken-2 border-primary white rounded-left">
														<span class="la la-calendar-o small-calender-icon"></span>
													</span>
                                                </div>
                                                <input type="text" name="search_date_from"
                                                       class="form-control bg-primary border-primary white rounded-right pickadate"
                                                       id="search_date_from" placeholder="Status Date From">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group input-group">
                                                <div class="input-group-prepend">
													<span
                                                        class="input-group-text bg-primary bg-darken-2 border-primary white rounded-left">
														<span class="la la-calendar-o small-calender-icon"></span>
													</span>
                                                </div>
                                                <input type="text" name="search_date_to"
                                                       class="form-control bg-primary border-primary white rounded-right pickadate"
                                                       id="search_date_to" placeholder="Status Date To">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <button type="button" id="search_filter_btn"
                                                    class="btn btn-outline-primary w-30"><i
                                                    class="la la-search"></i> Search
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--todo : page header end--}}
                            {{--todo: table--}}
                            <table class="table table-bordered datatable" id="datatable" style="z-index: 3;">
                                <thead>
                                <tr class="bg-primary white">
                                    <th class="border-primary border-darken-1"></th>
                                    <th class="border-primary border-darken-1">S No.</th>
                                    <th class="border-primary border-darken-1">Name</th>
                                    <th class="border-primary border-darken-1">Item Type</th>
                                    <th class="border-primary border-darken-1">Item SKU</th>
                                    <th class="border-primary border-darken-1">Sale Price</th>
                                    <th class="border-primary border-darken-1">Cost Price</th>
                                    <th class="border-primary border-darken-1">Created At</th>
                                    <th class="border-primary border-darken-1">Action</th>
                                </tr>
                                </thead>
                            </table>
                            {{--todo table end--}}
                            {{--todo : modal--}}
                            <div class="modal fade" id="add_item" data-backdrop="static" tabindex="-1"
                                 role="dialog" aria-labelledby="new_item" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title" id="add_mapping_title">Add New Item</h4>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form id="track_form"
                                                  action="{{route('admin.items.item_request')}}"
                                                  method="post" class="form-horizontal mb-1 justify-content-center"
                                                  novalidate="novalidate">
                                                @csrf
                                                <div class="col">
                                                    <div class="form-group">
                                                        <label for="shipper_city">Item Unit:
                                                            <span class="danger">*</span>
                                                        </label>
                                                        <fieldset class="form-group">
                                                            <select name="item_type" id="item_type"
                                                                    class="form-control select2">
{{--                                                                @foreach($item_units as $item)--}}
                                                                    <option value="1">Service</option>
                                                                <option value="2">Goods</option>
{{--                                                                @endforeach--}}
                                                            </select>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="name">
                                                                Name:
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   value="{{ old('name') }}" name="name">
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="name">
                                                                Sku ID:
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   value="{{ old('sku_id') }}" name="sku_id">
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="shipper_city">Item Unit:
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <fieldset class="form-group">
                                                                <select name="item_unit" id="item_unit"
                                                                        class="form-control select2">
                                                                    @foreach($item_units as $item)
                                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="title mb-2">
                                                    Sales Information
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="name">
                                                                Sale Price:
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <div class="input-group">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">SGD</span>
                                                                </div>
                                                                <input type="text" class="form-control"
                                                                       value="{{ old('sale_price') }}" name="sale_price">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="name">
                                                                Account:
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <fieldset class="form-group">
                                                                <select name="sale_type" id="sale_type"
                                                                        class="form-control select2">
                                                                    @foreach($sale_types as $sale_type)
                                                                        <option value="{{$sale_type->id}}">{{$sale_type->type}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="name">
                                                                Description:
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <textarea type="text" class="form-control"
                                                                      value="{{ old('sale_description') }}" name="sale_description"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="name">
                                                                Tax:
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <input type="text" class="form-control"
                                                                   value="{{ old('sale_tax') }}" name="sale_tax">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="title mb-2">
                                                    Cost Information
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="name">
                                                                Cost Price:
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <div class="input-group">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">SGD</span>
                                                                </div>
                                                                <input type="text" class="form-control"
                                                                       value="{{ old('cost_price') }}" name="cost">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="name">
                                                                Account:
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <fieldset class="form-group">
                                                                <select name="cost_type" id="cost_type"
                                                                        class="form-control select2">
                                                                    @foreach($cost_types as $cost_type)
                                                                        <option value="{{$cost_type->id}}">{{$cost_type->type}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="name">
                                                                Description:
                                                                <span class="danger">*</span>
                                                            </label>
                                                            <textarea type="text" class="form-control"
                                                                      value="{{ old('cost_description') }}" name="cost_description"></textarea>
                                                        </div>
                                                    </div>
{{--                                                    <div class="col">--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <label for="name">--}}
{{--                                                                Tax:--}}
{{--                                                                <span class="danger">*</span>--}}
{{--                                                            </label>--}}
{{--                                                            <input type="text" class="form-control"--}}
{{--                                                                   value="{{ old('cost_tax') }}" name="cost_tax">--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
                                                </div>
                                                <div class="col--md-6">
                                                    <button type="submit"
                                                            class="btn btn-outline-primary btn-min-width mt-1">
                                                        ADD
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {{--todo : modal end--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@section('css')
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/forms/selects/select2.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/pickers/pickadate/pickadate.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{asset('app-assets/css/plugins/pickers/daterange/daterange.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('app-assets/vendors/css/extensions/toastr.css')}}">

@endsection

@section('js')
    <script src="{{asset('app-assets/vendors/js/forms/select/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('/app-assets/vendors/js/forms/validation/jquery.validate.min.js')}}"
            type="text/javascript"></script>
    <script src="{{asset('js/datatable_buttons.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/pickers/pickadate/picker.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/pickers/pickadate/picker.date.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/pickers/pickadate/legacy.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/extensions/toastr.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/textarea/autosize.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('app-assets/vendors/js/forms/extended/inputmask/jquery.inputmask.bundle.min.js')}}"
            type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('#item_unit').prepend('<option value="" selected="selected"></option>').select2({
                placeholder: 'Select Unit',
                width: '100%',
                allowClear: true
            });
            $('#sale_type').prepend('<option value="" selected="selected"></option>').select2({
                placeholder: 'Sale',
                width: '100%',
                allowClear: true
            });
            $('#cost_type').prepend('<option value="" selected="selected"></option>').select2({
                placeholder: 'Cost of Goods Sold',
                width: '100%',
                allowClear: true
            });
            var selected_rows = [];
            var table = $('#datatable').DataTable({
                dom: '<" *inline-block"l><"pull-right"B>tipr',
                buttons: [
                    {
                        text: '<i class="la la-plus"></i>Add Item',
                        className: 'btn btn-primary add_item',
                        enabled: true,
                        action: function (e, dt, node, config) {
                            $('#add_item').modal('show');
                        }
                    },
                    {
                        extend: 'excel',
                        title: 'Employee Leaves',
                        className: 'btn btn-primary',
                        text: '<i class="la la-file-excel-o"></i> Excel',
                    },
                    'reset'
                ],
                scrollX: true, scrollY: '500px',
                lengthMenu: [[50, 100, 500, 1000, -1], [50, 100, 500, 1000, 'All']],
                pageLength: 50,
                autoWidth: false,
                pagingType: 'full_numbers',
                processing: true,
                language: {
                    processing: data_table_loader
                },
                "order": [[ 2, "asc" ]],
                serverSide: true,
                ajax: {
                    url: '{{ route('admin.items.item_list') }}',
                    data: function (d) {
                        d.search_date_from = $('#search_date_from').val();
                        d.search_date_to = $('#search_date_to').val();
                    }
                },
                columns: [
                    {data: 'id', orderable: false, searchable: false, class: 'text-center align-middle select p-1', targets: 0, render: function (data, type, row) {return '';}},
                    {
                        orderable: false,
                        searchable: false,
                        name: 'serial_number',
                        class: 'align-middle serial_number',
                        targets: 0,
                        render: function (data, type, row) {
                            return '';
                        }
                    },
                    {data: 'name', name: 'name', class: 'align-middle trax_id'},
                    {data: 'item_type', name: 'item_type', class: 'align-middle name',orderable: false},
                    {data: 'sku_id', name: 'sku_id', class: 'align-middle department'},
                    {data: 'cost_price', name: 'cost_price', class: 'align-middle designation'},
                    {data: 'sale_price', name: 'sale_price', class: 'align-middle department'},
                    {data: 'created_at', name: 'created_at', class: 'align-middle department'},
                    {data: 'action', name: 'action', class: 'align-middle action'},
                ],
                rowCallback: function (row, data, index) {
                    var info = table.page.info();
                    $('td:eq(1)', row).html(index + 1 + info.page * info.length);
                    $('td:eq(0)', row).addClass('select-checkbox');
                },

                initComplete: function() {
                    var search = $('<tr role="row" class="bg-primary bg-lighten-1 search"></tr>').appendTo(this.api().table().header());
                    var td = '<td style="padding:5px;" class="border-primary border-lighten-2"><fieldset class="form-group m-0 position-relative has-icon-right"></fieldset></td>';
                    var input = '<input type="text" class="form-control form-control-sm input-sm primary">';
                    var icon = '<div class="form-control-position primary"><i class="la la-search"></i></div>';
                    // var drop_select = '<select name="status_select" id="status_select" class="select2 form-control"></select>';
                    // var rider_status_select = '<select name="rider_status_select" id="rider_status_select" class="select2 form-control"></select>';
                    this.api().columns().every(function(column_id) {
                        var column = this;
                        var header = column.header();

                        if ($(header).is('.action') || $(header).is('.serial_number')) {
                            $(td).appendTo($(search));
                        }
                        else {
                            var current = $(input).appendTo($(search)).on('change', function() {
                                column.search($(this).val(), false, false, true).draw();
                            }).wrap(td).after(icon);

                            if (column.search()) {
                                current.val(column.search());
                            }
                        }
                    });
                    this.api().table().columns.adjust();
                }
            });
            //todo : leave request form submission
            $("#track_form").validate({
                errorClass: 'danger',
                successClass: 'success',
                errorPlacement: function (error, element) {
                    error.addClass('w-100').appendTo(element.parents('form'));
                },
                submitHandler: function (form) {
                    form.submit()
                }
            });
            $('#datatable tbody').on('click', 'tr td.select-checkbox', function() {

                var id = parseInt($(this).parent('tr').attr('id'));

                var index = $.inArray(id, selected_rows);

                if (index === -1) {
                    console.log('got it');
                    selected_rows.push(id);
                } else {
                    console.log('got');
                    selected_rows.splice(index, 1);
                }
            });
            // $("#track_form").submit(function (e) {
            //     e.preventDefault(); // prevent actual form submit
            //     var form = $(this);
            //     var url = form.attr('action'); //get submit url [replace url here if desired]
            //     $.ajax({
            //         type: "POST",
            //         url: url,
            //         data: form.serialize(), // serializes form input
            //     }).done(function (data) {
            //         // console.log(data);
            //         $('#leave_request').modal('hide');
            //
            //         if (data.status == 3) {
            //             toastr.error(data.message, 'Denied!', {
            //                 positionClass: 'toast-top-center',
            //                 containerId: 'toast-top-center'
            //             });
            //         } else if (data.status == 2) {
            //             toastr.success(data.success, 'Success!', {
            //                 positionClass: 'toast-top-center',
            //                 containerId: 'toast-top-center'
            //             });
            //         } else if (data.status == 1) {
            //             toastr.error(data.error, 'Error!', {
            //                 positionClass: 'toast-top-center',
            //                 containerId: 'toast-top-center'
            //             });
            //         } else if (data.status == 0) {
            //             toastr.error(data.error, 'Error!', {
            //                 positionClass: 'toast-top-center',
            //                 containerId: 'toast-top-center'
            //             });
            //         }
            //         $("#track_form")[0].reset();
            //     });
            // });
            //todo : leave request form submission end

            var search_date_from = $('#search_date_from').pickadate({
                firstDay: 1,
                clear: '',
                max: '{{ Carbon\Carbon::now() }}',
                // format: 'dd mmmm, yyyy',
                format: 'yyyy-mm-dd',
                selectYears: true,
                selectMonths: true,
                formatSubmit: 'yyyy-mm-dd 00:00:00',
                hiddenSuffix: '_formatted',
                onSet: function (context) {
                    if (context.select) {
                        $('#search_date_to').pickadate('picker').set('min', $('#search_date_from').pickadate('picker').get('select'));
                    }
                }
            });
            var search_date_to = $('#search_date_to').pickadate({
                firstDay: 1,
                clear: '',
                max: '{{ Carbon\Carbon::now() }}',
                // format: 'dd mmmm, yyyy',
                format: 'yyyy-mm-dd',
                selectYears: true,
                selectMonths: true,
                formatSubmit: 'yyyy-mm-dd 23:59:59',
                hiddenSuffix: '_formatted',
                onSet: function (context) {
                    if (context.select) {
                        $('#search_date_from').pickadate('picker').set('max', $('#search_date_to').pickadate('picker').get('select'));
                    }
                }
            });

            $('#search_filter_btn').on('click',function () {
                table.draw(true);
            });
        });
    </script>

@endsection
