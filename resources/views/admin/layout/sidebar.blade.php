<div class="main-menu menu-fixed menu-light menu-accordion menu-bordered menu-shadow"
     data-scroll-to-active="true">
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
            <li class=" nav-item"><a href="{{route('admin.dashboards.index')}}"><i class="la la-area-chart"></i>
                    <span class="menu-title" data-i18n="nav.dash.main">Dashboard</span></a>
            </li>
            <li class=" nav-item"><a href="#"><span class="menu-title" data-i18n="nav.vertical_nav.main"><i
                            class="la la-cart-plus"></i>Items</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="{{route('admin.items.items')}}">Items</a></li>
                    <li><a class="menu-item" href="">Price Lists</a></li>
                    <li><a class="menu-item" href="">Inventory Adjustments</a></li>
                    <li><a class="menu-item" href="">Notes</a></li>

                </ul>
            </li>

            <li><a class="menu-item" href=""><i class="la la-university"></i>Banking</a></li>

            <li class=" nav-item"><a href="#"><span class="menu-title" data-i18n="nav.vertical_nav.main"><i
                            class="la la-shopping-cart "></i>Sales</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="">Customers</a></li>
                    <li><a class="menu-item" href="">Estimates</a></li>
                    <li><a class="menu-item" href="">Retainer Invoice</a></li>
                    <li><a class="menu-item" href="">Sales Order</a></li>
                    <li><a class="menu-item" href="">Delivery Challans</a></li>
                    <li><a class="menu-item" href="">Invoices</a></li>
                    <li><a class="menu-item" href="">Payment Received</a></li>
                    <li><a class="menu-item" href="">Recurring Invoices</a></li>
                    <li><a class="menu-item" href="">Credit Notes</a></li>
                </ul>
            </li>
            <li class="divider"></li>

            <li class=" nav-item"><a href="#"><span class="menu-title" data-i18n="nav.vertical_nav.main"><i
                            class="las la-shopping-basket"></i>Purchases</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="">Vendor</a></li>
                    <li><a class="menu-item" href="">Expenses</a></li>
                    <li><a class="menu-item" href="">Recurring Expenses</a></li>
                    <li><a class="menu-item" href="">Purchase Orders</a></li>
                    <li><a class="menu-item" href="">Bills</a></li>
                    <li><a class="menu-item" href="">Payment Made</a></li>
                    <li><a class="menu-item" href="">Recurring Bills</a></li>
                    <li><a class="menu-item" href="">Vendor Credits</a></li>
                    <li><a class="menu-item" href="">Credit Notes</a></li>
                </ul>
            </li>
            <li class=" nav-item"><a href="#"><span class="menu-title" data-i18n="nav.vertical_nav.main"><i
                            class="las la-file-invoice"></i>Accountant</span></a>
                <ul class="menu-content">
                    <li><a class="menu-item" href="">Manual Journals</a></li>
                    <li><a class="menu-item" href="">Bulk Update</a></li>
                    <li><a class="menu-item" href="">Currency Adjustments</a></li>
                    <li><a class="menu-item" href="">Chart of Accounts</a></li>
                    <li><a class="menu-item" href="">Budgets</a></li>
                    <li><a class="menu-item" href="">Transaction Locking</a></li>
                </ul>
            </li>
            <li><a class="menu-item" href=""><i class="las la-file-image"></i>Reports</a></li>

        </ul>
    </div>
</div>
