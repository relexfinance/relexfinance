<?php
/**  @var Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(App\Item::class, function (Faker $faker) {
    return [
        'item_type' => $faker->randomElement(['0','1']),
        'name' => $faker->name,
        'sku_id' => $faker->countryCode,
        'item_unit' => $faker->numberBetween(1, 10),
        'sale_price' => $faker->randomElement(['15','20','25','30']),
        'sale_account_id' => $faker->numberBetween(1, 25),
        'sale_description' => $faker->realText('200'),
        'cost_price' => $faker->randomElement(['15','20','25','30']),
        'cost_account_id' => $faker->numberBetween(1, 25),
        'cost_description' => $faker->realText('200'),
        'extra_col' => $faker->randomLetter,
//        'created_at' => $faker->date('y-m-d','-3 years'),
//        'updated_at' => $faker->date('y-m-d','now')
    ];
});
