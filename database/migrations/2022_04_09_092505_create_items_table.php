<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->smallInteger('item_type')->index();
            $table->string('name');
            $table->string('sku_id')->unique()->index();
            $table->integer('item_unit');
            $table->float('sale_price')->nullable();
            $table->integer('sale_account_id')->index()->nullable();
            $table->longText('sale_description')->nullable();
            $table->float('cost_price')->nullable();
            $table->integer('cost_account_id')->index()->nullable();
            $table->longText('cost_description')->nullable();
            $table->longText('sale_tax')->nullable();
            $table->string('extra_col')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
