<?php

namespace App\Http\Controllers;

use App\CostAccountType;
use App\item;
use App\ItemUnit;
use App\SaleAccountType;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ItemController extends Controller
{
    public function item()
    {
        $items = Item::all();
        $item_units = ItemUnit::all();
        $sale_types = SaleAccountType::all();
        $cost_types = CostAccountType::all();
        return view('admin.inventory.items')->with(['items' => $items,'item_units'=>$item_units,'sale_types'=>$sale_types,'cost_types'=>$cost_types]);
    }
    public function item_list(Request $request)
    {
        $items = item::select('id','item_type','name','sku_id','item_unit','cost_price','sale_price','created_at')->get();

        if ($request->get('search_date_from') && $request->get('search_date_to')) {

            $from = $request->get('search_date_from');
            $to = $request->get('search_date_to');
            $stop_date = date('Y-m-d H:i:s', strtotime($to . ' +1 day'));
            $items->whereBetween('items.created_at', [$from, $stop_date]);
        }
        $datatables = Datatables::of($items)

            ->editColumn('item_type', function ($items) {
                if ($items->item_type == 0) {
                    return 'Goods';
                } else {
                    return 'Service';
                }
            })
            ->addColumn('action', function ($reminder_request) {
                $reminder_button = '<a href="javascript:void(0);" class="dropdown-item reminderMarkStatus" data-action="reminder"><i class="ft-plus-circle primary"></i> Edit </a>';
                $reminder_button1 = '<a href="javascript:void(0);" class="dropdown-item reminderMarkStatus" data-action="reminder"><i class="ft-plus-circle primary"></i> Delete</a>';


                    $dropdown = "
                        <div class='btn-group'>
                           <button type='button' class='btn btn-sm btn-success dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>Actions</button>
                            <div class='dropdown-menu dropdown-menu-sm'>";
                    $dropdown .= $reminder_button;
                    $dropdown .= $reminder_button1;
                    $dropdown .= "
                            </div>
                        </div>
                    ";

                    return $dropdown;
            });

        return $datatables->make(true);
    }
    public function item_request(Request $request)
    {
        $item = new Item();
        $item->item_type = $request->item_type;
        $item->name = $request->name;
        $item->sku_id = $request->sku_id;
        $item->item_unit = $request->item_unit;
        $item->sale_price = $request->sale_price;
        $item->sale_account_id = $request->sale_type;
        $item->sale_description = $request->sale_description;
        $item->cost_price = $request->cost_price;
        $item->cost_account_id = $request->cost_type;
        $item->cost_description = $request->cost_description;
        $item->sale_tax = $request->sale_tax;
        $item->save();
        return redirect()->back()->with('success','Item Added');
    }
}
