<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('admin.dashboard');
});

Route::prefix('admin')->name('admin.')->group(function () {
    Route::prefix('dashboard')->name('dashboards.')->group(function () {
        Route::get('', 'AdminController@index')->name('index');
    });
    Route::prefix('items')->name('items.')->group(function () {
        Route::get('', 'ItemController@item')->name('items');
        Route::get('item_list', 'ItemController@item_list')->name('item_list');
        Route::post('item_request', 'ItemController@item_request')->name('item_request');
    });
});
